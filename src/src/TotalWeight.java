import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class TotalWeight {

    static HashMap<String, Integer> sdf = new HashMap<>();

    public static void main(String[] args) {

        try {
            totalWeight("file4.txt");
            for (String key : sdf.keySet()) {
                System.out.println(key + " " + sdf.get(key));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void totalWeight(String file) throws IOException {

        BufferedReader in = new BufferedReader(new FileReader(file));
        String st;
        String tg;
        int it;

        while ((st = in.readLine()) != null) {
            String[] key = st.split(" ");
            tg = key[0];
            it = Integer.parseInt(key[1]);
            if (sdf.get(tg) != null) {
                it = it + sdf.get(tg);
            }
            sdf.put(tg, it);
        }


    }
}

