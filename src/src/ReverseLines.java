import java.io.*;
import java.util.Scanner;

public class ReverseLines {
    public static void main(String[] args) {

        Scanner consoleScanner = new Scanner(System.in);
        System.out.print("Input name file ");
        String source = consoleScanner.nextLine();
        System.out.print("Input name file");
        String destination = consoleScanner.nextLine();
        consoleScanner.close();

        try (Scanner in = new Scanner(new FileReader(source)); BufferedWriter out = new BufferedWriter(new FileWriter(destination))) {
            while (in.hasNext()) {
                String line = in.nextLine();
                out.write((new StringBuffer(line)).reverse().toString());
                out.newLine();
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("An I/O exception");
            e.printStackTrace();
        }
    }
}
