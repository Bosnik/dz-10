import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TextTransformation {
    public static void main(String[] args){

        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Input name file ");

            String source = sc.nextLine();
            System.out.print("Input name file ");

            String destination = sc.nextLine();
            Scanner in = new Scanner(new FileReader(source));
            FileWriter out = new FileWriter(destination);

            while (in.hasNext()) {
                String[] words = in.nextLine().split("\\s+");
                for (String word : words) {
                    if (word.length() % 2 == 0) {
                        String outWord = Character.toUpperCase(word.charAt(0)) + word.substring(1) + ", ";
                        out.write(outWord.toCharArray());
                    }
                }
            }
            in.close();
            out.close();
            sc.close();
        } catch (IOException e) {
            System.out.println("An I/O Exception");
            e.printStackTrace();
        }
    }

}
