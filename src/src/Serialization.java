import java.io.*;

public class Serialization {
    public static class Person implements Serializable {
        String firstName;
        String lastName;
        transient String fullName;
        final String finalString;
        Sex sex;
        transient PrintStream outputStream;
        static final long serialVersionUID = 42L;

        Person(String firstName, String lastName, Sex sex) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = String.format("%s, %s", lastName, firstName);
            this.finalString = "final string";
            this.sex = sex;
            this.outputStream = System.out;
        }

        @Override
        public String toString() {
            return firstName + " " +
                    lastName + ", " +
                    fullName + " " +
                    sex + " " +
                    finalString + " " +
                    serialVersionUID + "\n";
        }

        private void writeObject(java.io.ObjectOutputStream out) throws IOException {
            out.defaultWriteObject();
        }

        private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
            in.defaultReadObject();
            outputStream = System.out;
            this.fullName = String.format("%s, %s", lastName, firstName);
        }

    }

    enum Sex {
        MALE,
        FEMALE
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Person person = new Person("Hulk", "Hogan", Sex.MALE);

        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("tempfile.tmp"));
        out.writeObject(person);
        out.close();
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("tempfile.tmp"));
        Person newPerson = (Person) in.readObject();
        in.close();

        System.out.println(person.toString());
        System.out.println(newPerson.toString());

    }

}
