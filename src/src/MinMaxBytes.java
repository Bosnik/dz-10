import java.io.*;

public class MinMaxBytes {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream in = new FileInputStream(new File(reader.readLine()));
        int min = 300;
        int tmp;
        int max = 100;


        while (in.available() > 0) {
            tmp = in.read();
            if (tmp < min) {
                min = tmp;
            }
            while (in.available() > 0) {
                tmp = in.read();
                if (max > min)
                    max = tmp;
            }
        }

        System.out.println("Minumum " + max + "," + "Maximum " + min);
        reader.close();
        in.close();
    }

}




