import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class SearchInFile {
    static ArrayList<String> st = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        Scanner br = new Scanner(System.in);
        System.out.print("Input name file: \n");
        String namefile = br.next();
        System.out.print("Input name file ");
        String wd = br.next();


        read(namefile + ".txt", wd);
        for (String s : st)

        {
            System.out.println(s);
        }
    }


    public static void read(String file, String line) throws IOException {
        st.clear();
        BufferedReader in = new BufferedReader(new FileReader(file));
        String s;
        while ((s = in.readLine()) != null) {
            boolean a = s.contains(line);
            if (a == true) {
                st.add(s);
            }
        }
        in.close();
    }
}

